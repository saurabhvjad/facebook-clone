import React from 'react'
import { stories } from '../mocks'
import { DotsHorizontalIcon, VideoCameraIcon } from '@heroicons/react/solid'
import { SearchIcon } from '@heroicons/react/outline'
import Contact from './Contact'

function Widgets() {
  return (
    <div className="mt-5 hidden w-60 flex-col p-2 lg:inline-flex">
      <div className="mb-5 flex items-center justify-between text-gray-500">
        <h2 className="text-xl">Contacts</h2>
        <div className="flex space-x-2">
          <VideoCameraIcon className="h-6" />
          <SearchIcon className="h-6" />
          <DotsHorizontalIcon className="h-6" />
        </div>
      </div>

      {stories.map((contact) => (
        <Contact key={contact.id} src={contact.profile} name={contact.name} />
      ))}
    </div>
  )
}

export default Widgets
