import React from 'react'
import { stories } from '../mocks'
import StoryCard from './StoryCard'

function Stories() {
  return (
    <div className="mx-auto flex justify-center space-x-3 ">
      {stories.map(({ id, name, src, profile }) => (
        <StoryCard key={id} name={name} profile={profile} src={src} />
      ))}
    </div>
  )
}

export default Stories
